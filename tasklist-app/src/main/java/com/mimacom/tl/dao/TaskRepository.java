package com.mimacom.tl.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mimacom.tl.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

	@Query("SELECT t FROM Task t WHERE t.user = :user ORDER BY t.id")
	public List<Task> findByUser(@Param("user") String user);

}
