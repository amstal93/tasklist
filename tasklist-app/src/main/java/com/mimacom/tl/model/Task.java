package com.mimacom.tl.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="TASKS")
public class Task implements Serializable {

	private static final long serialVersionUID = -1202820572273694176L;

	@Id
	@GenericGenerator(name="task_id_generator" , strategy="increment")
	@GeneratedValue(generator="task_id_generator")
	private Long id;

	@Column(name="USER_ID")
	private String user;

	private String title;

	private String description;

	private Date dueDate;

	private Date completionDate;

	public boolean isDone() {
		return completionDate!=null;
	}

	public void updateValues(Task anotherTask) {
		if(anotherTask.getTitle()!=null) title = anotherTask.getTitle();
		if(anotherTask.getDescription()!=null) description = anotherTask.getDescription();
		if(anotherTask.getDueDate()!=null) dueDate = anotherTask.getDueDate();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", user=" + user + ", title=" + title + ", description=" + description + ", dueDate="
				+ dueDate + ", completionDate=" + completionDate + ", isDone()=" + isDone() + "]";
	}
}
