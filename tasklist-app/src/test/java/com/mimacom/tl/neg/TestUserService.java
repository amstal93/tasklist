package com.mimacom.tl.neg;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mimacom.tl.tools.Mocks;

@SpringBootTest
public class TestUserService {

	@Autowired UserService userService;

	@Test
	public void testPasswordCheck() {
		assertTrue(userService.checkPassword("acnalbaledpej", Mocks.jepdelablanca()));
	}
}
