package com.mimacom.tl.mvc;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import com.mimacom.tl.dao.TaskRepository;
import com.mimacom.tl.model.Task;
import com.mimacom.tl.tools.Mocks;
import com.mimacom.tl.tools.Tools;

@SpringBootTest
@Transactional
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class TestTaskController {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private TaskRepository taskRepository;
	
	@Value("${tasklist.formLogin}")
	private Boolean formLogin;

	@Test
	public void testListTasks() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		mockMvc.perform(MockMvcRequestBuilders.get("/tasks").session(session).accept(MediaType.APPLICATION_JSON)).
			andExpect(MockMvcResultMatchers.status().isOk()).
			andExpect(MockMvcResultMatchers.content().json(Tools.toJsonString(taskRepository.findByUser("guest"))));
	}

	@Test
	public void testCreateTask() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		
		Task expected = Tools.clone(Mocks.goShopping(), Task.class);
		expected.setId(4l);
		expected.setUser("guest");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/tasks").session(session).
			contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(Tools.toJsonString(Mocks.goShopping()))).
			andExpect(MockMvcResultMatchers.status().isCreated()).
			andExpect(MockMvcResultMatchers.content().json(Tools.toJsonString(expected)));
	}

	@Test
	public void testUpdateTask() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		
		Task modified = Mocks.celebrate();
		modified.setDueDate(Tools.tomorrow());
		
		mockMvc.perform(MockMvcRequestBuilders.put("/tasks/2").session(session).
			contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(Tools.toJsonString(modified))).
			andExpect(MockMvcResultMatchers.status().isOk()).
			andExpect(MockMvcResultMatchers.content().json(Tools.toJsonString(modified)));
	}

	@Test
	public void testFinalizeTask() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		mockMvc.perform(MockMvcRequestBuilders.post("/tasks/1/finalized").session(session).accept(MediaType.APPLICATION_JSON)).
			andExpect(MockMvcResultMatchers.status().isOk()).
			andExpect(MockMvcResultMatchers.jsonPath("$.done", Matchers.is(true)));
	}

	@Test
	public void testDeleteTask() throws Exception {
		MockHttpSession session = Tools.getGuestSession(mockMvc, formLogin);
		mockMvc.perform(MockMvcRequestBuilders.delete("/tasks/1").session(session).accept(MediaType.APPLICATION_JSON)).
			andExpect(MockMvcResultMatchers.status().isOk());
		assertTrue(taskRepository.findById(1l).isEmpty());
	}
}