package com.mimacom.tl.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mimacom.tl.model.Task;
import com.mimacom.tl.tools.Mocks;
import com.mimacom.tl.tools.Tools;

@DataJpaTest
public class TestTaskRepository {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private EntityManager entityManager;

	@Autowired
	private TaskRepository taskRepository;

	@Test
	public void testInjectedComponentsAreNotNull() {
		assertNotNull(dataSource);
		assertNotNull(jdbcTemplate);
		assertNotNull(entityManager);
		assertNotNull(taskRepository);
	}

	@Test
	public void testDefaultTasksExist() {
		assertNotNull(taskRepository.findAll());
		assertEquals(3, StreamSupport.stream(taskRepository.findAll().spliterator(), false).count());
	}

	@Test
	public void testFindTasksByUser() {
		assertEquals(2, StreamSupport.stream(taskRepository.findByUser("guest").spliterator(), false).count());
	}

	@Test
	public void testGettingTaskById() {
		assertNotNull(taskRepository.findById(1l).orElseThrow());
	}

	@Test
	public void testDeleting() {
		taskRepository.deleteById(1l);
		assertTrue(taskRepository.findById(1l).isEmpty());
	}

	@Test
	public void testInserting() {
		Long insertedId = taskRepository.save(Mocks.goShopping()).getId();
		assertNotNull(insertedId);
		assertFalse(taskRepository.findById(insertedId).isEmpty());
	}

	@Test
	public void testUpdating() {
		Task original = Mocks.goShopping();
		assertNull(original.getDueDate());
		Long insertedId = taskRepository.save(original).getId();
		
		Date dueDate = Tools.tomorrow();
		
		Task retrieved = taskRepository.findById(insertedId).get();
		retrieved.setDueDate(dueDate);
		taskRepository.save(retrieved);
		
		Task modified = taskRepository.findById(insertedId).get();
		assertEquals(dueDate, modified.getDueDate());
	}
}
