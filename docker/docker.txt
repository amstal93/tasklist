
# Purga total de Docker
root > docker container stop $(docker container ls -aq) && docker system prune -af --volumes

# Crear la red
> sudo docker network create tasklist-network

# Inspeccionar la red
> sudo docker network inspect tasklist-network

# Crear un contenedor Alpine para inspeccionar la red
> sudo docker run -dit --name tasklist-ash --network tasklist-network alpine ash

# Conectarse al contenedor para hacer ping a la base de datos:
> sudo docker container attach tasklist-ash

/ # ping -c 2 tasklist-ash
/ # ping -c 2 google.com

# Desconectarse del contenedor usando la secuencia de escape: CTRL + p CTRL + q

# Compilar la imagen de la aplicacion
> cd application
> sudo docker build -t tasklist/application .

# Compilar la imagen de la base de datos
> cd database
> sudo docker pull postgres:13
> sudo docker build -t tasklist/database .

# Listar las imagenes generadas
> sudo docker image list
REPOSITORY             TAG                 IMAGE ID            CREATED              SIZE
tasklist/database      latest              47408352b1b3        15 seconds ago       314MB
tasklist/application   latest              04e4010008c0        About a minute ago   672MB
openjdk                11                  f2f9cd527eff        2 days ago           628MB
alpine                 latest              d6e46aa2470d        3 days ago           5.57MB
postgres               13                  c96f8b6bc0d9        12 days ago          314MB

# Ejecutar la base de datos
> sudo docker run --name tasklist-database --network tasklist-network -p 5432:5432 -e POSTGRES_PASSWORD=2c5bD6dg01Faa1d8 -d tasklist/database

# Cargar el esquema y datos de la base de datos
> sudo docker exec -i tasklist-database psql -U tasklist -f /02_schema.sql
> sudo docker exec -i tasklist-database psql -U tasklist -f /03_data.sql

# Probar que se llega por ping desde la red a la base de datos
> sudo docker container attach tasklist-ash
/ # ping -c 2 tasklist-database
# Desconectarse del contenedor usando la secuencia de escape: CTRL + p CTRL + q

# Ejecutar la aplicacion
> sudo docker run --name tasklist-application --network tasklist-network -p 8080:8080 -d tasklist/application
