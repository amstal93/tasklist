#!/bin/bash

# Create network
docker network create tasklist-network

# Build database
cd database
docker build -t tasklist/database .

# Build application
cd ..
cd application
docker build -t tasklist/application .
